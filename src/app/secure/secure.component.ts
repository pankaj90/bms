import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Constant } from '../shared/constant';
//import { Constant } from '../Admin/Common/constant';
//import { SignalRConnection, SignalR, BroadcastEventListener } from 'ng2-signalr';
//import { NotificationUsers } from '../Admin/Common/NotificationUsers';
//import { NotificationService } from '../Admin/Common/Notification.Service';

@Component({
  selector: 'identify-secure',
  templateUrl: 'secure.component.html',
  styleUrls: ['secure.component.css'],
  //providers:[NotificationService]
})
export class SecureComponent implements OnInit {

  //private _connection: SignalRConnection;

  constructor(private _router: Router,
     private _constant: Constant, 
    private route: ActivatedRoute
    //, private _signalR: SignalR,
    //private _notificationService:NotificationService
  ) { }

  LoggedInUser : string = "Admin";
  LoggedInUserRole : string;
  ngOnInit(): void {
    // this._connection = this.route.snapshot.data['connection'];
    // this._connection
    //   .start()
    //   .then(() => {
    //   })
    //   .catch(err => console.log('Error while establishing connection :('));
    // this.activateListner();
    // this.LoggedInUser = this._constant.GetLoggedInUserName();
    // this.LoggedInUserRole = this._constant.GetLoggedInUserRole();
  }
  //NotificationUsers = new NotificationUsers();
  // activateListner() {
  //   // 1.create a listener object
  //   let onMessageSent$ = new BroadcastEventListener<NotificationUsers>('SendToAll');
  //   // 2.register the listener
  //   this._connection.listen(onMessageSent$);
  //   // 3.subscribe for incoming messages
  //   onMessageSent$.subscribe((data) => {
  //     this.NotificationUsers = data;
  //   });
  //   this._signalR.connect().then(() => {
  //     this._notificationService.getInitialNotifications().subscribe();      
  //   });
  // }
  logout() {
    this._constant.DeleteLocalStorage("currentActiveUser");
    this._router.navigate(['Login']);
  }

  updateUserStatus(userId:number){
    //this._notificationService.updateNotificationStatusOfUser(userId).subscribe();
  }

}
