﻿import { Component, OnInit } from "@angular/core";
import { BookFieldsService } from "./booksFields.service";
import { IBookFields } from './bookFields.interface'
import { Subscription } from "rxjs";


@Component({
    selector: 'BookFields',
    templateUrl: 'booksFields.component.html',
    styles: ['.whiteColor { color: white; } .table-bordered > thead > tr > th, .table-bordered > tbody > tr > td {width:1%}'],
    providers: [BookFieldsService]
})
export class BooksFieldsComponent implements OnInit {
    constructor(private _categoryService: BookFieldsService
    ) {

    }
    bookFields: IBookFields[];
    busy: Subscription;
    searchKey = "";
    sortColumn = "Value";
    isAscending = true;
    currentPageActive: number;
    totalPages: number;

    ngOnInit() {
        this.currentPageActive = 1;
        this.getData();
    }
    SortByColumn(columnName: string) {
        this.sortColumn = columnName;
        this.isAscending = !this.isAscending;
    }
    getData() {
        this.busy =  this._categoryService.getBookFieldsList(this.currentPageActive,10,this.searchKey).subscribe(data => 
              {
                  let d = data.json();
                   this.bookFields = d.Result;
                   console.log(this.bookFields);
                   this.totalPages = Math.ceil(d.TotalRecords/10);
              })
    }

    Delete(id: number) {
                var _this = this;
        if (confirm('Are you sure, you want to delete this record?')) {
             _this._categoryService.deleteFields(id).
                 subscribe(data => {
                     let d = data.json();
                     if (d.Result == 1) {
                        //Constant.SuccessMessageAlert(data.Message);
                        _this.getData();
                    } else {
                        //Constant.ErrorMessageAlert(data.Message);
                    }
                });
        }
    }
    pageChangeNotify(selectedPage: number) {
         this.currentPageActive = selectedPage;
         this._categoryService.getBookFieldsList(this.currentPageActive, 10, this.searchKey).subscribe(data => this.bookFields = data.json().Result)
    }
    ngOnDestroy() {
         if (this.busy !== undefined)
             this.busy.unsubscribe();
    }

    onSearchKeyChange() {
        this.getData();
    }
}
