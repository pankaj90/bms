import {Injectable} from "@angular/core";
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { ILogin} from './login.interface'
import { Constant } from "../shared/constant";

@Injectable()
export class LoginService {

    constructor(private _http: Http) {
    }

    checkLogin(data: ILogin){
        let body = new URLSearchParams();
        Object.keys(data).forEach(function (key) {
            body.append(key, data[key]);
        });
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._http
            .post(Constant.DomainPath + 'api/BMS/Login?'+ body, { headers: headers });
    }
}