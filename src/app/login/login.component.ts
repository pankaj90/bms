import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import {ILogin, IUserResponse} from './login.interface';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { Constant } from '../shared/constant';

@Component({
  selector: 'identify-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService],
  encapsulation : ViewEncapsulation.None
})
export class LoginComponent implements OnInit, OnDestroy {

  CurrentUser:IUserResponse;
  constructor(private _loginService: LoginService, private _router:Router, private _constant:Constant) { }

  LoginId : string;
  Password:string;

  ngOnInit() {
  }
  private iLogin = {} as ILogin;
  CheckLogin()
  {
    this.iLogin = 
    {
      LoginId : this.LoginId,
      Password : this.Password 
    }
    this._loginService.checkLogin(this.iLogin).subscribe(d => 
      {
        let data = d.json();
        if(data.StatusId == 0)
        {
          alert(data.Message);
        }
        else
        {
          //alert(data.Message);
          this.CurrentUser = data.Result;
          this._constant.SaveLocalStorageForUser(this.CurrentUser);
          setTimeout(() => {
            this._router.navigate(['Admin']);
          }, 500);
        }
      })
  }
  
  ngOnDestroy(){
    //this.subscription.unsubscribe();
  }


}
