import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from "@angular/core";

@Component({
    selector: 'pagging',
    templateUrl: './pagging.component.html'
})

export class PaggingComponent implements OnInit {
    constructor() {
    }
    ngOnInit() {
    }
    @Input() pageSize: number;
    totalPages = 0;
    @Input() currentPage: number = 1;
    createRange(number: number) {
        this.totalPages = number;
        var items: any[] = [];
        if (this.currentPage >= 1 || this.currentPage < 8) {
            if (number <= 10) {
                for (var i = 1; i <= number; i++) {
                    items.push(i);
                }
            }
            else if (this.currentPage >= 9 && this.currentPage < this.totalPages) {
                for (var i = 1; i <= number; i++) {
                    if (i == 1) {
                        items.push('...');
                    }
                    else if (i == this.totalPages) {
                        items.push(i);
                    }
                    else {
                        if (this.currentPage == i) {
                            items.push(i);
                        }
                        else if (this.currentPage < this.totalPages && i > this.currentPage - 8 && i < this.currentPage) {
                            items.push(i);
                        }
                        else if (this.currentPage + 1 == i && this.currentPage != this.totalPages) {
                            items.push('...')
                        }
                    }
                }
            }
            else {
                for (var i = 1; i <= number; i++) {
                    if (i <= 8) {
                        items.push(i);
                    }
                    else if (i == 9) {
                        items.push("...");
                    }
                    else if (i == number) {
                        items.push(i);
                    }
                }
            }
        }
        return items;
    }


    @Output()
    pageChanged: EventEmitter<number> = new EventEmitter<number>();
    onPageChanged(pageNumber: number) {
        if (typeof (pageNumber) === 'number') {
            this.currentPage = pageNumber;
            this.pageChanged.emit(this.currentPage);
        }
    }

    onNextPageChanged(isLast) {
        if (isLast) {
            this.currentPage = this.totalPages;
            this.pageChanged.emit(this.currentPage);
        }
        else if (this.currentPage != this.totalPages) {
            this.currentPage++;
            this.pageChanged.emit(this.currentPage);
        }
    }

    onPreviousPageChanged(isFirst) {
        if (isFirst) {
            this.currentPage = 1;
            this.pageChanged.emit(this.currentPage);
        }
        else if (this.currentPage != 1) {
            this.currentPage--;
            this.pageChanged.emit(this.currentPage);
        }
    }
}
