﻿import {Injectable} from "@angular/core";
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { IBookFields } from './bookFields.interface';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Constant } from "src/app/shared/constant";


@Injectable()
export class BookFieldsService {

    constructor(private _http: Http, private _httpClient : HttpClient) {
    }

    getBookFieldsList(pageNumber:number, pageSize:number, whereClause?:string) {

        var body = 'PageNumber='+pageNumber+'&PageSize=' + pageSize +'&WhereClause='+whereClause;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
         return this._http.post(Constant.DomainPath + 'api/BMS/GetBookFields?'+body, { headers: headers }
         );
    }

    deleteFields(id: number) {
        var body = 'TableType=2&Id=' + id;
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + 'api/BMS/Delete?'+ body, { headers: headers });
    }

    addBookFields(data: any) {
        let body = new URLSearchParams();
        Object.keys(data).forEach(function (key) {
            body.append(key, data[key]);
        });
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + '/api/BMS/SaveBookFields?'+ body, { headers: headers })
    }

    getBookFields(fieldId: number) {
        let body = new URLSearchParams();
        body.append('Id', fieldId.toString());
        body.append('TableType', '2');

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + '/api/BMS/GetEntityDetail?'+ body, { headers: headers })
    }

    getAllBooks(Id: number) {
        var headers = new HttpHeaders();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this._httpClient.post(Constant.DomainPath + "api/BMS/GetAllBooks?Id=" + Id, { headers: headers });
    }
    uploadFiles(formData: FormData) {
        let headers = new Headers();
        return this._http
        .post(Constant.DomainPath + 'api/BMS/MediaUpload', formData, { headers: headers });
    }
}