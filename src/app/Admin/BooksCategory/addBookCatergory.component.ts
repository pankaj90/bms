﻿import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import { BooksCategoryService } from "./booksCategory.service";
import { Constant } from "src/app/shared/constant";

@Component({
    selector: 'addBookcatergory',
    templateUrl: 'addBookcatergory.component.html',
    providers: [BooksCategoryService]
})
export class AddBookCatergoryComponent implements OnInit {
    constructor(private _categoryService: BooksCategoryService, private router: Router, private route: ActivatedRoute) {
    }
    public Id: number = 0;
    public CategoryCode: string;
    public CategoryName: string;
    public Title: string;
    //busy: Subscription;
    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params['id'] && parseInt(params['id']) > 0) {
                this.Id = parseInt(params['id']);
                this.GetCategoryDetail(this.Id);
                this.Title = "Update Book Category";
            }
            else {
                this.Title = "Add Book Category";

            }
        });
    }

    GetCategoryDetail(id: number){
        this._categoryService.getCategory(id).subscribe(data => 
        {
            let d = data.json();
            this.CategoryCode = d.Result.CategoryCode,
            this.CategoryName = d.Result.CategoryName,
            this.Id = d.Result.Id
        });
    }

    SaveCategory(){
        var objCategory = {
            ID: this.Id,
            BookCode: this.CategoryCode,
            BookTitle: this.CategoryName
        }
        this._categoryService.addCategory(objCategory)
            .subscribe(data => {
                let d = data.json();                
                if (data.json().StatusId  == 1) {
                    alert(d.Message);
                    //Constant.SuccessMessageAlert(d.Message);
                    this.router.navigate(['/Admin/BookCategory']);
                }
                else{
                    alert(d.Message);
                    //Constant.ErrorMessageAlert(data.Message);
                }
            });
    }
    ngOnDestroy(){
    //     if(this.busy !== undefined)
    //         this.busy.unsubscribe();
       }
}
