import { Routes } from "@angular/router";
import { AddBookCatergoryComponent } from "./BooksCategory/addBookCatergory.component";
import { BooksCategoryComponent } from "./BooksCategory/booksCategory.component";
import { BooksFieldsComponent } from "./BooksFields/booksFields.component";
import { AddBookFieldsComponent } from "./BooksFields/addBookFields.component";

export const adminRoutes: Routes = [
        { path: '', redirectTo: 'Reports', pathMatch: 'full' },
        { path: 'BooksCategory', component: BooksCategoryComponent },
        { path: 'BookCategory/Add', component: AddBookCatergoryComponent },
        { path: 'BookCategory/Edit/:id', component: AddBookCatergoryComponent },
        { path: 'BookFields', component: BooksFieldsComponent },
        { path: 'AddBookFields', component: AddBookFieldsComponent }
];
