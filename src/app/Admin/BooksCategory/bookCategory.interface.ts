﻿export interface IBookCategory {
    Id : number;
    BookCode: string;
    BookTitle: string;
}
