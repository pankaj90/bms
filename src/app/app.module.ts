import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { AdminModule } from './Admin/admin.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { SecureComponent } from './secure/secure.component';
import { FormsModule } from '@angular/forms';
import { BooksCategoryComponent } from './Admin/BooksCategory/booksCategory.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { PaggingComponent } from './shared/pagging.component';
import { AddBookCatergoryComponent } from './Admin/BooksCategory/addBookCatergory.component';
import { BooksFieldsComponent } from './Admin/BooksFields/booksFields.component';
import { BookFieldsService } from './Admin/BooksFields/booksFields.service';
import { AddBookFieldsComponent } from './Admin/BooksFields/addBookFields.component';
import { FieldValidityDirective } from './shared/fieldValidity.directive';
import { LoginComponent } from './login/login.component';
import { Constant } from './shared/constant';
import { AuthGuardService } from './shared/AuthGuardService';

const appRoutes: Routes = [
  { path: 'Login', component: LoginComponent },
  {     
      path: 'Admin', 
      component: SecureComponent, 
      children:
      [
          { path: '', redirectTo: 'BookCategory', pathMatch: 'full' },
          { path: 'BookCategory', component: BooksCategoryComponent },
          { path: 'BookCategory/Add', component: AddBookCatergoryComponent },
          { path: 'BookCategory/Edit/:id', component: AddBookCatergoryComponent },
          { path: 'BookFields', component: BooksFieldsComponent },
          { path: 'BookFields/Add', component: AddBookFieldsComponent },
          { path: 'BookFields/Edit/:id', component: AddBookFieldsComponent }
      ],
      //resolve : {connection:ConnectionResolver},
      canActivate:[AuthGuardService] 
  },
  { path: '', redirectTo: 'Login', pathMatch: 'full' },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent, SecureComponent, BooksCategoryComponent, PaggingComponent, 
    AddBookCatergoryComponent, 
    BooksFieldsComponent,AddBookFieldsComponent, FieldValidityDirective, LoginComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule, FormsModule,
    HttpModule, HttpClientModule,
  ],
  providers: [Constant, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
