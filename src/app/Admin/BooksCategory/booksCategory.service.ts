﻿import {Injectable} from "@angular/core";
import { Http, Headers, Response, URLSearchParams, RequestOptions} from '@angular/http';
import { IBookCategory } from './bookCategory.interface';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Constant } from "src/app/shared/constant";


@Injectable()
export class BooksCategoryService {

    constructor(private _http: Http, private _httpClient : HttpClient) {
    }

    getCategoriesList(pageNumber:number, pageSize:number, whereClause?:string) {

        var body = 'PageNumber='+pageNumber+'&PageSize=' + pageSize +'&WhereClause='+whereClause;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
         return this._http.post(Constant.DomainPath + 'api/BMS/GetBooksCategories?'+body, { headers: headers }
         );
    }

    deleteCategory(id: number) {
        var body = 'TableType=1&Id=' + id;
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + 'api/BMS/Delete?'+ body, { headers: headers });
    }

    addCategory(data: any) {
        let body = new URLSearchParams();
        Object.keys(data).forEach(function (key) {
            body.append(key, data[key]);
        });
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + '/api/BMS/SaveCategory?'+ body, { headers: headers })
    }

    getCategory(categoryId: number) {
        let body = new URLSearchParams();
        body.append('Id', categoryId.toString());
        body.append('TableType', '1');

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
         return this._http
             .post(Constant.DomainPath + '/api/BMS/GetEntityDetail?'+ body, { headers: headers })
    }
}