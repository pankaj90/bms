export interface ILogin {
    LoginId: string;
    Password: string;
}

export interface IUserResponse{
    UserId:number,
    Role:string,
    Token:string,
    UserName:string,
    EmailAddress:string,
    Password : string
}

export class EmailRequest {
    EmailId: string;
    Password: string;
}
