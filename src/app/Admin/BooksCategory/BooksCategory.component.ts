﻿import { Component, OnInit } from "@angular/core";
import { BooksCategoryService } from "./booksCategory.service";
import { IBookCategory } from './bookCategory.interface'
import { Subscription } from "rxjs";


@Component({
    selector: 'BooksCategory',
    templateUrl: 'booksCategory.component.html',
    styles: ['.whiteColor { color: white; }'],
    providers: [BooksCategoryService]
})
export class BooksCategoryComponent implements OnInit {
    constructor(private _categoryService: BooksCategoryService
    ) {

    }
    bookCategories: IBookCategory[];
    busy: Subscription;
    searchKey = "";
    sortColumn = "Value";
    isAscending = true;
    currentPageActive: number;
    totalPages: number;

    ngOnInit() {
        this.currentPageActive = 1;
        this.getData();
    }
    SortByColumn(columnName: string) {
        this.sortColumn = columnName;
        this.isAscending = !this.isAscending;
    }
    getData() {
        this.busy =  this._categoryService.getCategoriesList(this.currentPageActive,10,this.searchKey).subscribe(data => 
              {
                  let d = data.json();
                   this.bookCategories = d.Result;
                   console.log(this.bookCategories);
                   this.totalPages = Math.ceil(d.TotalRecords/10);
              })
    }

    Delete(id: number) {
                var _this = this;
        if (confirm('Are you sure, you want to delete this record?')) {
             _this._categoryService.deleteCategory(id).
                 subscribe(data => {
                     let d = data.json();
                     if (d.Result == 1) {
                        //Constant.SuccessMessageAlert(data.Message);
                        _this.getData();
                    } else {
                        //Constant.ErrorMessageAlert(data.Message);
                    }
                });
        }
    }
    pageChangeNotify(selectedPage: number) {
         this.currentPageActive = selectedPage;
         this._categoryService.getCategoriesList(this.currentPageActive, 10, this.searchKey).subscribe(data => this.bookCategories = data.json().Result)
    }
    ngOnDestroy() {
         if (this.busy !== undefined)
             this.busy.unsubscribe();
    }

    onSearchKeyChange() {
        this.getData();
    }
}
