﻿import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import { BookFieldsService } from "./booksFields.service";
import { IBookFields, BookFields } from "./bookFields.interface";

@Component({
    selector: 'addBookfields',
    templateUrl: 'addBookfields.component.html',
    styleUrls:['addBookFields.component.css'],
    providers: [BookFieldsService]
})
export class AddBookFieldsComponent implements OnInit {
    constructor(private _fieldService: BookFieldsService, private router: Router, private route: ActivatedRoute) {
    }
    public Id: number = 0;
    public Title: string;
    public bookFields: BookFields = {};
    public books: any[] = [];
    SelectedFiles = 0;
    //busy: Subscription;
    ngOnInit() {
        this._fieldService.getAllBooks(0).subscribe((data : any) => 
        {
            this.books = data.Result
        });
        this.route.params.subscribe(params => {
            if (params['id'] && parseInt(params['id']) > 0) {
                this.Id = parseInt(params['id']);
                this.GetBookFieldsDetail(this.Id);
                this.Title = "Update Book Fields";
            }
            else {
                this.bookFields.BookId =0; 
                this.Title = "Add Book Fields";

            }
        });
    }

    GetBookFieldsDetail(id: number){
        this._fieldService.getBookFields(id).subscribe(data => 
        {
            let d = data.json();
            this.bookFields = d.Result,
            this.Id = d.Result.Id
        });
    }

    formData : FormData;
    SaveBookFields(){
        if(this.bookFields.BookId === 0)
        {
            alert('Please select a valid book');
            return false;
        }

        if (this.fileToUpload !== undefined || this.fileToUpload.length > 0) {
            this.formData = new FormData();
            this.formData.append('fileToUpload', this.fileToUpload[0], this.fileToUpload[0].name);
        }
      
        var objCategory = this.bookFields;
        this._fieldService.addBookFields(objCategory)
            .subscribe(data => {
                let d = data.json();                
                if (data.json().StatusId  == 1) {
                    alert(d.Message);
                    this.router.navigate(['/Admin/BookFields']);
                }
                else{
                    alert(d.Message);
                }
            });
    }
    fileToUpload: any[] = [];
    fileChange(event) {
        let fileList: FileList = event.target.files;
        if(fileList.length > 1 )
        {
          alert("you can select one file at a time.");
          return false;
        }
        if (fileList.length > 0) {
          this.SelectedFiles = fileList.length;
          this.fileToUpload[0] = fileList[0];
          if(
              ((this.fileToUpload[0].type.indexOf('jpeg') > -1)
              || (this.fileToUpload[0].type.indexOf('jpg') > -1)
              || (this.fileToUpload[0].type.indexOf('png') > -1))
              || (this.fileToUpload[0].type.indexOf('pdf') > -1))
          {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(this.fileToUpload[0]);        
            oFReader.onload = function () {
              //document.getElementById("frontImagePath").setAttribute('src',oFReader.result);
            };
            //this.frontImageFormat = this.fileToUpload[0].name.substr(this.fileToUpload[0].name.lastIndexOf('.')+1,this.fileToUpload[0].name.length).toUpperCase();
            //Constant.InfoMessageAlert('front image selected!');
          }
          else{
              alert("Please select valid file i.e (JPEG/JPG/PNG/PDF)");
            return false;
          }
        }
      }
      Upload(){
          this._fieldService.uploadFiles(this.formData);
      }
    ngOnDestroy(){
    //     if(this.busy !== undefined)
    //         this.busy.unsubscribe();
       }
}
