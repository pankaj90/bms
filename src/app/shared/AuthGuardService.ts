import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Constant } from './constant';

@Injectable()
export class AuthGuardService implements CanActivate 
{
constructor(private _constant : Constant, private _router : Router)
{
}
canActivate(): boolean 
  {
    let token = this._constant.GetLoggedInUserToken();
    if(token == null)
    {
        this._router.navigate(["Login"]);
    }
    return token == null ? false : true;

  }
}
